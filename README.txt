This modules allows attaching a profile2 to products.
It creates a commerce profile2 field.
When the field is attached to a product type entity a form with
an e-mail field and profile2 form is presented on checkout.
Upon checkout completion a user account is created for each profile2
product and e-mails the new account holder.
